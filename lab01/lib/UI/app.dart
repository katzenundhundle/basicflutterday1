import 'package:flutter/material.dart';
import 'dart:convert';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  int counter = 0;
  @override
  Widget build(BuildContext context) {
    var app = new MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Image Viewer"),
        ),
        body: Text("Display image here, counter = $counter"),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              setState(() {
                counter += 1;
              });
            },
            backgroundColor: Colors.blue,
            child: const Icon(Icons.add)),
      ),
    );
    return app;
  }
}
