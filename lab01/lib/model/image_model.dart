class Image {
  int? albumId;
  int? id;
  String? title;
  String? url;
  String? thumbnailUrl;
  Image({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});
}
